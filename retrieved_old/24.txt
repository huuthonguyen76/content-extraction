1 Backup your media and data. Reformatting the Android device will erase all of the data stored on it. Make sure that you have backups of any important files, pictures, documents, videos, or anything else you want to keep. Your app purchases are tied to your Google account, and will be available for re-download when you sign in to your formatted device. See this guide for details on backing up your device's settings. See this guide for details on transferring files from your device to your computer.
2 Open your device's Settings menu. You can access this from the Settings app, or by pressing your phone's Menu button and then selecting Settings.
3 Tap "Backup & reset". This will open the menu that allows you to reset your phone.
4 Tap "Factory data reset". You will be asked to confirm that you want top reset the phone. once you confirm, the phone will start the reset process. When the process is complete, your phone will be like it was new out of the box.
1 Boot your phone into Recovery Mode. If your Android device will not boot normally, you can perform a factory reset from the Recovery boot menu. Press and hold the correct buttons while your phone is powered off. The buttons vary from device to device.[1] Nexus devices - Volume Up, Volume Down, and Power Samsung devices - Volume Up, Home, and Power Moto X - Volume Down, Home, and Power Other devices generally use Volume Down and Power, while other phones with hard UIs can use Power and Home, since Power and Volume Down in devices with true home buttons will make it boot to factory test mode.
2 Select "recovery". Use the volume buttons to scroll through the menu options. Select "recovery" from the list, and then press the Power button to select it.
3 Select "wipe data/factory reset". Confirm that you want to reset the phone by selecting "Yes - delete all user data". Your phone will now begin the reset process.
Search.
Add New Question How do I get my contacts and apps onto the formatted phone?
wikiHow Contributor.
Log in with your Google account - all contacts and apps can be synced by saying "Yes" to the prompt "Do you want to sync all contacts and apps?".
Flag as duplicate.
Thanks! Yes No.
Not Helpful 2 Helpful 10.
What do I do if my phone and messaging have stopped?
wikiHow Contributor.
Go Settings, Backup and Reset. Often the Factory Reset command is in Settings, Security.
Flag as duplicate.
Thanks! Yes No.
Not Helpful 1 Helpful 7.
What should I do if I don't back up my factory data before reformatting my Android phone?
wikiHow Contributor.
The factory reset will delete all the data from the internal storage (though it will not touch anything on the SD card, if you have one). There's nothing you can do about it once it's done. If you don't want to lose anything, make sure to back it up beforehand.
Flag as duplicate.
Thanks! Yes No.
Not Helpful 1 Helpful 7.
My phone is hanging up. What should I do?
wikiHow Contributor.
Try resetting it to factory condition. If this does not work, take it to a service center.
Flag as duplicate.
Thanks! Yes No.
Not Helpful 0 Helpful 0.
Unanswered Questions.
I can't access my phone because I forgot my password, what do I do?
Answer this question Flag as... Flag as...
My android phone unfortunately stopped all processes and media, how can I troubleshoot this?
Answer this question Flag as... Flag as...
My Android ZTE won't load past the "Powered by Android" screen. How can I fix that problem?
Answer this question Flag as... Flag as...
What do I do if the response is "no command?".
Answer this question Flag as... Flag as...
What will I do if the google play store stops?
Answer this question Flag as... Flag as...
Show more unanswered questions.
Ask a Question.
200 characters left.
Submit.
Already answered.
Not a question.
Bad question.
Other.
If this question (or a similar one) is answered twice in this section, please click here to let us know.
How to Close Apps on Android.
How to Play PSP Games on Android with the PPSSPP App.
How to Check for Updates on Your Android Phone.
How to Close Apps on a Samsung Galaxy.
How to Download a Torrent With Android.
How to Reset a Samsung Galaxy S3.
How to Transfer Photos from Android to Computer.
How to Turn Off Auto Correct on an Android.
How to Upgrade an Android Device from Version 2.3 to 4.0 Manually.
How to Share Apps on Android Bluetooth.
↑ http://androidflagship.com/8011-enter-recovery-mode-moto-x.
