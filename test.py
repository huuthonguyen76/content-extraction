import requests
import time
import json
import numpy as np
import matplotlib.pyplot as plt
import math

from sympy import Point, Line
from bs4 import BeautifulSoup, Tag, Comment
from typing import List
from sklearn.cluster import k_means
from sklearn.feature_extraction.text import CountVectorizer
from scipy.spatial.distance import cosine

from internal.helper.soup_helper import SoupHelper
from internal.helper.string_helper import StringHelper



def cosine_similarity(s_1, s_2):
    tfidf = CountVectorizer(stop_words='english')
    X = tfidf.fit_transform([s_1, s_2])
    return cosine(X[0].toarray(), X[1].toarray())

def LCS(X, Y):
    m = len(X)
    n = len(Y)
    # An (m+1) times (n+1) matrix
    C = [[0] * (n + 1) for _ in range(m + 1)]
    for i in range(1, m+1):
        for j in range(1, n+1):
            if X[i-1] == Y[j-1]: 
                C[i][j] = C[i-1][j-1] + 1
            else:
                C[i][j] = max(C[i][j-1], C[i-1][j])
    return C

def backTrack(C, X, Y, i, j):
    if i == 0 or j == 0:
        return ""
    elif X[i-1] == Y[j-1]:
        return backTrack(C, X, Y, i-1, j-1) + X[i-1]
    else:
        if C[i][j-1] > C[i-1][j]:
            return backTrack(C, X, Y, i, j-1)
        else:
            return backTrack(C, X, Y, i-1, j)

def get_LCS(X, Y):
    m = len(X)
    n = len(Y)
    C = LCS(X, Y)
    return backTrack(C, X, Y, m, n)

import nltk, string
from sklearn.feature_extraction.text import TfidfVectorizer

nltk.download('punkt') # if necessary...


stemmer = nltk.stem.porter.PorterStemmer()
remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)

def stem_tokens(tokens):
    return [stemmer.stem(item) for item in tokens]

'''remove punctuation, lowercase, stem'''
def normalize(text):
    return stem_tokens(nltk.word_tokenize(text.lower().translate(remove_punctuation_map)))

vectorizer = TfidfVectorizer(tokenizer=normalize, stop_words='english')

def cosine_sim(text1, text2):
    tfidf = vectorizer.fit_transform([text1, text2])
    return ((tfidf * tfidf.T).A)[0,1]

l_result = []
l_old_result = []
l_execution_time = []
for i in range(100, 200, 1):
    # if i == 10 or i == 18:
    #     continue

    # if i != 18:
    #     continue

    with open('./retrieved_old/{}.txt'.format(i), 'r') as f_1:
    #with open('./CEED/Article/{}.txt'.format(i), 'r') as f_1:
        with open('{}/{}/{}.txt'.format('CEED', 'Content', i), 'r') as f_2:

            gold_content = f_2.read()
            predicted_content = f_1.read()
            # with open('{}/{}/{}.txt'.format('CEED', 'Article', i), 'r') as f_3:
            #     old_ce_content = f_3.read()

            new_ce_acc = cosine_sim(gold_content, predicted_content)
            
            print('New CE',i,  new_ce_acc)

            l_result.append(new_ce_acc)

print('New CE acc:', sum(l_result) / len(l_result))            


