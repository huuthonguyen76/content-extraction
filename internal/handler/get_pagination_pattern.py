import tornado.ioloop
import tornado.web
import json

from internal.task.pagination_pattern_detection import PaginationPatternDetection


class GetPaginationPatternHandler(tornado.web.RequestHandler):
    def post(self):
        data = json.loads(self.request.body)
        url = data['url']
        pagination_pattern_detection = PaginationPatternDetection(url)
        pattern = pagination_pattern_detection.training_category_pagination()
        print(pattern)
        self.write(pattern)
