import requests
import re
import traceback
import numpy as np
import matplotlib.pyplot as plt

from sympy import Point, Line
from sklearn.cluster import k_means
from bs4 import BeautifulSoup, Comment

from internal.helper.soup_helper import soup_helper
from internal.helper.string_helper import string_helper


class Extractor():
    def __init__(self):
        pass

    def get_article_content_by_url(self, url):
        try:
            return self.__get_article_content(url)

        except Exception:
            print(traceback.format_exc())

        return None

    def get_article_content_by_text(self, content):
        try:
            return self.__get_article_content_by_algorithm(content)

        except Exception:
            print(traceback.format_exc())

        return None

    def __get_url_content(self, url):
        response = requests.get(url)
        raw_content = response.content
        content = raw_content.decode(response.encoding)
        return content

    def __get_article_content(self, url):
        content = self.__get_url_content(url)
        content = content.replace('\x00', '')

        if url.find('news.zing.vndasd') > -1:
            return self.__get_article_content_by_selector(content, '.the-article-body')

        else:
            return self.__get_article_content_by_algorithm(content)

    def __get_article_content_by_selector(self, content, selector):
        soup = BeautifulSoup(content, 'html.parser')
        selected_soup = soup.select(selector)

        return selected_soup[0].getText()

    def __get_article_content_by_algorithm(self, content, debug=False):
        def get_node_score(node):
            score = 0
            for character in node:
                score += ord(character)

            return score

        # Instead of scoring on node name.
        # We can score on position instead.
        def get_path_score(path):
            score = 0
            l_node = path.split(' ')
            l_node.reverse()
            
            for index, node in enumerate(l_node):
                node_score = get_node_score(node)

                # Normalize the score
                score += (node_score)
            score = int(score)
            return score

        def remove_number_path(path):
            return re.sub('(_[0-9]+)', '', path)

        # Improve for removing ambiguous category group
        # Fix remove a lot of group o newyorktime
        def get_real_parent_anchor_path(path, similarity_paths):
            def get_parent_path(path, similarity_path):
                l_path_part = path.split(' ')
                l_similarity_part = similarity_path.split(' ')

                l_path_part.reverse()
                l_similarity_part.reverse()
                l_parent_part = l_path_part[:]
                level = 0

                while level != len(l_path_part) - 1:
                    if l_path_part[level] != l_similarity_part[level]:
                        l_parent_part.reverse()
                        return ' '.join(l_parent_part), level

                    level += 1
                    l_parent_part.pop(0)

                return path, 0

            current_level = 0
            current_parent_path = path
            for i in similarity_paths:
                if i == path:
                    continue

                inferred_parent_path, inferred_level = get_parent_path(path, i)
                if current_level < inferred_level:
                    current_level = inferred_level
                    current_parent_path = inferred_parent_path

            return current_parent_path

        content = string_helper.replace_unclose_br_tag(content)
        content = string_helper.replace_unclose_img_tag(content)
        content = string_helper.replace_unclose_input_tag(content)

        soup = BeautifulSoup(content, 'html.parser')

        [s.extract() for s in soup('script')]
        [s.extract() for s in soup('iframe')]
        [s.extract() for s in soup('header')]
        [s.extract() for s in soup('label')]
        [s.extract() for s in soup('option')]
        [s.extract() for s in soup('head')]

        [s.extract() for s in soup('footer')]
        [s.extract() for s in soup('style')]
        [s.extract() for s in soup('noscript')]
        [s.extract() for s in soup('video')]
        [s.extract() for s in soup('videolist')]
        [s.extract() for s in soup('source')]
        [s.extract() for s in soup('track')]

        comments = soup.findAll(text=lambda text: isinstance(text, Comment))
        [comment.extract() for comment in comments]

        l_content_length = []
        l_content = []
        l_path = []
        d_text_path = soup_helper.get_text_paths(soup)

        l_delete_path = []
        for text_path in d_text_path:
            current_content = d_text_path[text_path]
            if len(current_content) < 20:
                l_delete_path.append(text_path)
        
        for delete_path in l_delete_path:
            del d_text_path[delete_path]

        # for text_path in d_text_path:
        #     print(d_text_path[text_path])
        #     print("==========")

        # exit()

        d_unnumber_path_dict = {}
        for text_path in d_text_path:
            current_content = d_text_path[text_path]

            if text_path.find('a_') > -1:
                unnumber_path = remove_number_path(text_path)
                if unnumber_path not in d_unnumber_path_dict:
                    d_unnumber_path_dict[unnumber_path] = []

                d_unnumber_path_dict[unnumber_path].append(text_path)

        main_content_sub_soups = []
        for unnumber_path in d_unnumber_path_dict:
            if len(d_unnumber_path_dict[unnumber_path]) < 2:
                continue

            current_anchor_paths = d_unnumber_path_dict[unnumber_path]
            for path in current_anchor_paths:
                real_parent_path = get_real_parent_anchor_path(
                    path, current_anchor_paths
                )
                main_content_sub_soups.append(soup_helper.get_soup_by_path(soup, real_parent_path))

        for sub_soup in main_content_sub_soups:
            if len(sub_soup.getText()) < 150:
                sub_soup.extract()

        [s.extract() for s in soup('a')]

        d_text_path = soup_helper.get_text_paths(soup)

        for text_path in d_text_path:
            current_content = d_text_path[text_path]
            path = soup_helper.convert_text_path_to_path(text_path)
            if path.strip() == '':
                continue
            l_path.append(path)
            l_content_length.append(len(current_content))
            l_content.append(current_content)

        X = []
        plot_x = []
        plot_y = []
        for index, element in enumerate(l_content_length):
            X.append([get_path_score(l_path[index]), element])
            plot_x.append(get_path_score(l_path[index]))
            plot_y.append(element)

        X = np.array(X)

        plot_score = []
        plot_num = []
        highest_score = 0
        total_case = len(X) if len(X) < 10 else 10
        for i in range(1, total_case, 1):
            current_kmeans = k_means(X, i)
            if i == 1:
                highest_score = current_kmeans[2]
                num_rate = highest_score / 10
            
            plot_score.append(round(current_kmeans[2], 2))
            plot_num.append(i * num_rate)

        p1 = Point(plot_num[0], plot_score[0], evaluate=False)
        p2 = Point(plot_num[-1], plot_score[-1], evaluate=False)
        line = Line(p1, p2, evaluate=False)
        highest_score = 0
        k = 1

        for i, _ in enumerate(plot_score):
            if i > 1:
                A = Point((plot_num[i-1], plot_score[i-1]), evaluate=False)
                B = (plot_num[i-2], plot_score[i-2])
                distance = float(line.distance(A))
                if highest_score < distance:
                    highest_score = distance
                    k = i

        kmeans = k_means(X, k)
        plot_cent_x = kmeans[0][:, 0]
        plot_cent_y = kmeans[0][:, 1]

        l_label = kmeans[1]
        d_label = {}

        for index, label in enumerate(l_label):
            if label not in d_label:
                d_label[label] = []
            d_label[label].append(l_content_length[index])

        candidate_label = 0
        highest_score = 0
        for label in d_label:
            score = sum(d_label[label])
            if score > highest_score:
                highest_score = score
                candidate_label = label

        l_result = []
        l_result_path = []
        for index, label in enumerate(l_label):
            if label == candidate_label:
                l_result.append(l_content[index])
                l_result_path.append(l_path[index])

        if debug:
            plt.scatter(plot_x, plot_y, s=3)
            plt.scatter(plot_cent_x, plot_cent_y, color='red', s=4)
            plt.show()

        return '\n'.join(l_result)

    def get_list_content_by_url(self, url):
        def get_node_score(node):
            score = 0
            for character in node:
                score += ord(character)

            return score

        # Instead of scoring on node name.
        # We can score on position instead.
        def get_path_score(path):
            score = 0
            l_node = path.split(' ')
            l_node.reverse()
            
            for index, node in enumerate(l_node):
                node_score = get_node_score(node)

                # Normalize the score
                score += (node_score)
            score = int(score)
            return score

        def remove_number_path(path):
            return re.sub('(_[0-9]+)', '', path)

        def get_text_only(txt):
            txt = txt.replace('\r\n', '')
            txt = txt.replace(' ', '')
            return txt

        # Improve for removing ambiguous category group
        # Fix remove a lot of group o newyorktime
        def get_real_parent_anchor_path(path, similarity_paths):
            def get_parent_path(path, similarity_path):
                l_path_part = path.split(' ')
                l_similarity_part = similarity_path.split(' ')

                l_path_part.reverse()
                l_similarity_part.reverse()
                l_parent_part = l_path_part[:]
                level = 0

                while level != len(l_path_part) - 1:
                    if l_path_part[level] != l_similarity_part[level]:
                        l_parent_part.reverse()
                        return ' '.join(l_parent_part), level

                    level += 1
                    l_parent_part.pop(0)

                return path, 0

            current_level = 0
            current_parent_path = path
            for i in similarity_paths:
                if i == path:
                    continue

                inferred_parent_path, inferred_level = get_parent_path(path, i)
                if current_level < inferred_level:
                    current_level = inferred_level
                    current_parent_path = inferred_parent_path

            return current_parent_path

        content = self.__get_url_content(url)
        content = string_helper.replace_unclose_br_tag(content)
        content = string_helper.replace_unclose_img_tag(content)
        content = string_helper.replace_unclose_input_tag(content)

        soup = BeautifulSoup(content, 'html.parser')

        [s.extract() for s in soup('script')]
        [s.extract() for s in soup('iframe')]
        [s.extract() for s in soup('header')]
        [s.extract() for s in soup('label')]
        [s.extract() for s in soup('option')]
        [s.extract() for s in soup('head')]

        [s.extract() for s in soup('footer')]
        [s.extract() for s in soup('style')]
        [s.extract() for s in soup('noscript')]
        [s.extract() for s in soup('video')]
        [s.extract() for s in soup('videolist')]
        [s.extract() for s in soup('source')]
        [s.extract() for s in soup('track')]

        comments = soup.findAll(text=lambda text: isinstance(text, Comment))
        [comment.extract() for comment in comments]

        d_text_path = soup_helper.get_text_paths(soup)

        l_delete_path = []
        for text_path in d_text_path:
            current_content = d_text_path[text_path]
            if len(current_content) < 5:
                l_delete_path.append(text_path)
        
        for delete_path in l_delete_path:
            del d_text_path[delete_path]

        d_unnumber_path_dict = {}
        for text_path in d_text_path:
            current_content = d_text_path[text_path]

            if text_path.find('a_') > -1:
                unnumber_path = remove_number_path(text_path)
                if unnumber_path not in d_unnumber_path_dict:
                    d_unnumber_path_dict[unnumber_path] = []

                d_unnumber_path_dict[unnumber_path].append(text_path)

        d_result = {}
        d_result_score = {}
        for unnumber_path in d_unnumber_path_dict:
            if len(d_unnumber_path_dict[unnumber_path]) < 2:
                continue

            d_result[unnumber_path] = []
            d_result_score[unnumber_path] = []
            current_anchor_paths = d_unnumber_path_dict[unnumber_path]
            d_track = {}
            for path in current_anchor_paths:
                real_parent_path = get_real_parent_anchor_path(
                    path, current_anchor_paths
                )

                if real_parent_path in d_track:
                    continue

                d_track[real_parent_path] = True

                txt_only = get_text_only(soup_helper.get_soup_by_path(soup, real_parent_path).getText())
                d_result[unnumber_path].append(soup_helper.get_soup_by_path(soup, real_parent_path))
                d_result_score[unnumber_path].append(len(txt_only))

        highest_score = 0
        highest_path = ''

        for unnumber_path in d_result_score:
            if sum(d_result_score[unnumber_path]) > highest_score:
                highest_score = sum(d_result_score[unnumber_path])
                highest_path = unnumber_path

        final_content = ''
        for i in d_result[highest_path]:
            final_content += i.getText().strip() + '\n\n'
            
        return final_content

    def get_list_content_by_content(self, content):
        def get_node_score(node):
            score = 0
            for character in node:
                score += ord(character)

            return score

        # Instead of scoring on node name.
        # We can score on position instead.
        def get_path_score(path):
            score = 0
            l_node = path.split(' ')
            l_node.reverse()
            
            for index, node in enumerate(l_node):
                node_score = get_node_score(node)

                # Normalize the score
                score += (node_score)
            score = int(score)
            return score

        def remove_number_path(path):
            return re.sub('(_[0-9]+)', '', path)

        def get_text_only(txt):
            txt = txt.replace('\r\n', '')
            txt = txt.replace(' ', '')
            return txt

        # Improve for removing ambiguous category group
        # Fix remove a lot of group o newyorktime
        def get_real_parent_anchor_path(path, similarity_paths):
            def get_parent_path(path, similarity_path):
                l_path_part = path.split(' ')
                l_similarity_part = similarity_path.split(' ')

                l_path_part.reverse()
                l_similarity_part.reverse()
                l_parent_part = l_path_part[:]
                level = 0

                while level != len(l_path_part) - 1:
                    if l_path_part[level] != l_similarity_part[level]:
                        l_parent_part.reverse()
                        return ' '.join(l_parent_part), level

                    level += 1
                    l_parent_part.pop(0)

                return path, 0

            current_level = 0
            current_parent_path = path
            for i in similarity_paths:
                if i == path:
                    continue

                inferred_parent_path, inferred_level = get_parent_path(path, i)
                if current_level < inferred_level:
                    current_level = inferred_level
                    current_parent_path = inferred_parent_path

            return current_parent_path

        content = string_helper.replace_unclose_br_tag(content)
        content = string_helper.replace_unclose_img_tag(content)
        content = string_helper.replace_unclose_input_tag(content)

        soup = BeautifulSoup(content, 'html.parser')

        [s.extract() for s in soup('script')]
        [s.extract() for s in soup('iframe')]
        [s.extract() for s in soup('header')]
        [s.extract() for s in soup('label')]
        [s.extract() for s in soup('option')]
        [s.extract() for s in soup('head')]

        [s.extract() for s in soup('footer')]
        [s.extract() for s in soup('style')]
        [s.extract() for s in soup('noscript')]
        [s.extract() for s in soup('video')]
        [s.extract() for s in soup('videolist')]
        [s.extract() for s in soup('source')]
        [s.extract() for s in soup('track')]

        comments = soup.findAll(text=lambda text: isinstance(text, Comment))
        [comment.extract() for comment in comments]

        d_text_path = soup_helper.get_text_paths(soup)

        l_delete_path = []
        for text_path in d_text_path:
            current_content = d_text_path[text_path]
            if len(current_content) < 5:
                l_delete_path.append(text_path)
        
        for delete_path in l_delete_path:
            del d_text_path[delete_path]

        d_unnumber_path_dict = {}
        for text_path in d_text_path:
            current_content = d_text_path[text_path]

            if text_path.find('a_') > -1:
                unnumber_path = remove_number_path(text_path)
                if unnumber_path not in d_unnumber_path_dict:
                    d_unnumber_path_dict[unnumber_path] = []

                d_unnumber_path_dict[unnumber_path].append(text_path)

        # for unnumber_path in d_unnumber_path_dict:
        #     print(unnumber_path)
        #     print("==============")
        #     print("==============")
        #     print("==============")

        #     for text_path in d_unnumber_path_dict[unnumber_path]:
        #         print(soup_helper.get_soup_by_path(soup, text_path))


        #     print("****************")
        #     print("****************")
            

        d_result = {}
        d_result_score = {}
        for unnumber_path in d_unnumber_path_dict:
            if len(d_unnumber_path_dict[unnumber_path]) < 2:
                continue

            d_result[unnumber_path] = []
            d_result_score[unnumber_path] = []
            current_anchor_paths = d_unnumber_path_dict[unnumber_path]
            d_track = {}
            for path in current_anchor_paths:
                real_parent_path = get_real_parent_anchor_path(
                    path, current_anchor_paths
                )

                if real_parent_path in d_track:
                    continue

                d_track[real_parent_path] = True

                txt_only = get_text_only(soup_helper.get_soup_by_path(soup, real_parent_path).getText())
                d_result[unnumber_path].append(soup_helper.get_soup_by_path(soup, real_parent_path))
                d_result_score[unnumber_path].append(len(txt_only))

        highest_score = 0
        highest_path = ''

        for unnumber_path in d_result_score:
            if sum(d_result_score[unnumber_path]) > highest_score:
                highest_score = sum(d_result_score[unnumber_path])
                highest_path = unnumber_path

        final_content = ''
        for i in d_result[highest_path]:
            final_content += i.getText().strip() + '\n\n'

        return final_content

