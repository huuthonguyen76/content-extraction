import tornado.ioloop
import tornado.web

from internal.handler.get_article_content import GetArticleContentHandler
from internal.handler.get_pagination_pattern import GetPaginationPatternHandler


def start_app():
    return tornado.web.Application([
        ('/get_article_content', GetArticleContentHandler),
        ('/get_pagination_pattern', GetPaginationPatternHandler),
    ])


if __name__ == "__main__":
    app = start_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
