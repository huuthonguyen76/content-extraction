import tornado.ioloop
import tornado.web
import json

from internal.task.extractor import Extractor


class GetArticleContentHandler(tornado.web.RequestHandler):
    def post(self):
        url = self.get_argument('url', '')
        extractor = Extractor()
        content = extractor.get_article_content_by_url(url)
        self.write(content)

