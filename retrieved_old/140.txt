Men's 5000m.
August 17 - Final.
GBR.
Mo Farah.
13:03.30.
USA.
Paul Kipkemoi Chelimo.
13:03.90.
ETH.
Hagos Gebrhiwet.
13:04.35.
4CAN.
Mohammed Ahmed.
13:05.94.
5USA.
Bernard Lagat.
13:06.78.
6GBR.
Andrew Butchart.
13:08.61.
7BRN.
Albert Rop.
13:08.79.
8UGA.
Joshua Cheptegei.
13:09.17.
9BRN.
Birhanu Yemataw Balew.
13:09.26.
10.
ERI.
Abrar Osman.
13:09.56.
11.
USA.
Hassan Mead.
13:09.81.
12.
ETH.
Dejen Gebremeskel.
13:15.91.
13.
RSA.
Elroy Gelant.
13:17.47.
14.
AUS.
Brett Robinson.
13:32.30.
15.
PER.
David Torrence.
13:43.12.
16.
ETH.
Muktar Edris.
DSQ.
17.
UGA.
Phillip Kipyeko.
13:24.66.
18.
KEN.
Isiah Kiplangat Koech.
13:25.15.
19.
KEN.
Caleb Mwangangi Ndiku.
13:26.63.
20.
AZE.
Hayle Ibrahimov.
13:27.11.
21.
AUS.
Patrick Tiernan.
13:28.48.
22.
GER.
Florian Orth.
13:28.88.
23.
ERI.
Aron Kifle.
13:29.45.
24.
ERI.
Hiskel Tewelde.
13:30.23.
24.
ESP.
Illias Fifa.
13:30.23.
26.
JAM.
Kemoy Campbell.
13:30.32.
27.
UGA.
Jacob Kiplimo.
13:30.40.
28.
KEN.
Charles Muneria.
13:30.95.
29.
JPN.
Suguru Osako.
13:31.45.
30.
ESP.
Adel Mechaal.
13:34.42.
31.
MAR.
Younés Essalhi.
13:41.41.
32.
LES.
Namakwe Nkhasi.
13:41.92.
33.
BEL.
Bashir Abdi.
13:42.83.
34.
BDI.
Olivier Irabaruta.
13:44.08.
35.
AUS.
Sam McEntee.
13:50.55.
36.
MAR.
Soufiane Boukantar.
13:56.55.
37.
CAN.
Lucas Bruchet.
14:02.02.
38.
GER.
Richard Ringer.
14:05.01.
39.
TUR.
Ali Kaya.
14:05.34.
40.
GBR.
Tom Farrell.
14:11.65.
41.
KSA.
Moukhled Al-Outaibi.
14:18.48.
42.
JPN.
Kota Murayama.
14:26.72.
43.
KSA.
Tariq Ahmed Al-Amri.
14:26.90.
44.
ESP.
Antonio Abadía.
14:33.20.
45.
MAW.
Kefasi Chitsala.
14:52.89.
46.
NEP.
Hari Kumar Rimal.
14:54.42.
47.
SOM.
Mohamed Mohamed.
14:57.84.
48.
SOL.
Rosefelo Siosi.
15:47.76.
49.
MYA.
San Naing.
15:51.05.
50.
STP.
Romario Martins Leitao.
15:53.32.
BRN.
Zouhair Aouad.
DNF.
Men's 5000m, Round 1.
August 17 - Final.
1USA.
Paul Kipkemoi Chelimo.
13:19.54 Q.
2ETH.
Muktar Edris.
13:19.65 Q.
3ETH.
Dejen Gebremeskel.
13:19.67 Q.
4BRN.
Birhanu Yemataw Balew.
13:19.83 Q.
5GBR.
Andrew Butchart.
13:20.08 Q.
6CAN.
Mohammed Ahmed.
13:21.00 q.
7RSA.
Elroy Gelant.
13:22.00 q.
8ERI.
Abrar Osman.
13:22.56 q.
9AUS.
Brett Robinson.
13:22.81 q.
10.
PER.
David Torrence.
13:23.20 q.
11.
ETH.
Hagos Gebrhiwet.
13:24.65 Q.
12.
UGA.
Phillip Kipyeko.
13:24.66.
13.
BRN.
Albert Rop.
13:24.95 Q.
14.
KEN.
Isiah Kiplangat Koech.
13:25.15.
15.
GBR.
Mo Farah.
13:25.25 Q.
16.
UGA.
Joshua Cheptegei.
13:25.70 Q.
17.
USA.
Bernard Lagat.
13:26.02 Q.
18.
KEN.
Caleb Mwangangi Ndiku.
13:26.63.
19.
AZE.
Hayle Ibrahimov.
13:27.11.
20.
AUS.
Patrick Tiernan.
13:28.48.
21.
GER.
Florian Orth.
13:28.88.
22.
ERI.
Aron Kifle.
13:29.45.
23.
ERI.
Hiskel Tewelde.
13:30.23.
23.
ESP.
Illias Fifa.
13:30.23.
25.
JAM.
Kemoy Campbell.
13:30.32.
26.
UGA.
Jacob Kiplimo.
13:30.40.
27.
KEN.
Charles Muneria.
13:30.95.
28.
JPN.
Suguru Osako.
13:31.45.
29.
USA.
Hassan Mead.
13:34.27.
30.
ESP.
Adel Mechaal.
13:34.42.
31.
MAR.
Younés Essalhi.
13:41.41.
32.
LES.
Namakwe Nkhasi.
13:41.92.
33.
BEL.
Bashir Abdi.
13:42.83.
34.
BDI.
Olivier Irabaruta.
13:44.08.
35.
AUS.
Sam McEntee.
13:50.55.
36.
MAR.
Soufiane Boukantar.
13:56.55.
37.
CAN.
Lucas Bruchet.
14:02.02.
38.
GER.
Richard Ringer.
14:05.01.
39.
TUR.
Ali Kaya.
14:05.34.
40.
GBR.
Tom Farrell.
14:11.65.
41.
KSA.
Moukhled Al-Outaibi.
14:18.48.
42.
JPN.
Kota Murayama.
14:26.72.
43.
KSA.
Tariq Ahmed Al-Amri.
14:26.90.
44.
ESP.
Antonio Abadía.
14:33.20.
45.
MAW.
Kefasi Chitsala.
14:52.89.
46.
NEP.
Hari Kumar Rimal.
14:54.42.
47.
SOM.
Mohamed Mohamed.
14:57.84.
48.
SOL.
Rosefelo Siosi.
15:47.76.
49.
MYA.
San Naing.
15:51.05.
50.
STP.
Romario Martins Leitao.
15:53.32.
BRN.
Zouhair Aouad.
DNF.
Men's 5000m, Round 1, Heat 1.
August 17 - Final.
1ETH.
Hagos Gebrhiwet.
13:24.65 Q.
2BRN.
Albert Rop.
13:24.95 Q.
3GBR.
Mo Farah.
13:25.25 Q.
4UGA.
Joshua Cheptegei.
13:25.70 Q.
5USA.
Bernard Lagat.
13:26.02 Q.
6KEN.
Caleb Mwangangi Ndiku.
13:26.63.
7AZE.
Hayle Ibrahimov.
13:27.11.
8ERI.
Aron Kifle.
13:29.45.
9ESP.
Illias Fifa.
13:30.23.
10.
JAM.
Kemoy Campbell.
13:30.32.
11.
UGA.
Jacob Kiplimo.
13:30.40.
12.
KEN.
Charles Muneria.
13:30.95.
13.
USA.
Hassan Mead.
13:34.27.
14.
MAR.
Younés Essalhi.
13:41.41.
15.
LES.
Namakwe Nkhasi.
13:41.92.
16.
BEL.
Bashir Abdi.
13:42.83.
17.
BDI.
Olivier Irabaruta.
13:44.08.
18.
AUS.
Sam McEntee.
13:50.55.
19.
CAN.
Lucas Bruchet.
14:02.02.
20.
GER.
Richard Ringer.
14:05.01.
21.
KSA.
Moukhled Al-Outaibi.
14:18.48.
22.
JPN.
Kota Murayama.
14:26.72.
23.
NEP.
Hari Kumar Rimal.
14:54.42.
24.
SOM.
Mohamed Mohamed.
14:57.84.
25.
SOL.
Rosefelo Siosi.
15:47.76.
