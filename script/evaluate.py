from internal.task.extractor import Extractor

extractor = Extractor()

for i in range(50, 201):
    with open('./CEED/HTML/{}.html'.format(i), 'r') as f:
        content = f.read().decode('utf-8')

        try:
            with open('./Predicted/{}.html'.format(i), 'wb+') as f_2:
                extracted_content = extractor.get_list_content_by_content(content)
                f_2.write(extracted_content.encode('utf-8'))
        except Exception as e:
            print(e)
