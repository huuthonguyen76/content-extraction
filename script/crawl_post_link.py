import requests

from bs4 import BeautifulSoup


class ExtractArticleLink():
    def __init__(self):
        pass

    def get_article_link(self, article_selector, category_pattern, start_page, end_page):
        l_link = []
        for i in range(start_page, end_page):
            current_category_link = category_pattern.replace('{integer}', str(i))
            response = requests.get(current_category_link)
            soup = BeautifulSoup(response.content, 'html.parser')
            l_soup_link = soup.select(article_selector)
            for soup_link in l_soup_link:
                l_link.append(soup_link['href'])
        return l_link

a = ExtractArticleLink()
# print(a.get_article_link(article_selector='.news-title a', category_pattern='http://www.24h.com.vn/cong-nghe-thong-tin-c55.html?vpage={integer}', start_page=1, end_page=3))
http://soha.vn/kham-pha.htm
print(a.get_article_link())