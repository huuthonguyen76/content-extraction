Relevance.
Relevance ranks synonyms and suggests the best matches based on how closely a synonym’s sense matches the sense you selected.
A-Z.
Complexity.
Complexity sorts synonyms based on their difficulty. Adjust it higher to choose from words that are more complex.
-+.
Length.
Length ranks your synonyms based on character count.
-+.
lists.
blocks.
Common.
Common words appear frequently in written and spoken language across many genres from radio to academic journals.
Informal.
Informal words should be reserved for casual, colloquial communication.
adj.
expected.
Synonyms for anticipated.
adj.
expected.
foreseen.
star.
certain.
star.
likely.
star.
predictable.
star.
sure.
star.
prepared for.
star.
Sad Words.
Usage Notes.
Despite claims that anticipate should only be used to mean “to perform (an action) or respond to (a question, etc.) in advance” or “to forestall,” it has been used widely since the 18th century as a synonym for expect, often with an implication of pleasure:  We anticipate a large turnout at the next meeting.  This use is standard in all types of speech and writing.
More words related to anticipated.
coming.
adj. approaching, promising.
about to happen.
advancing.
almost on one.
anticipated.
aspiring.
at hand.
certain.
close.
converging.
deserving.
docking.
drawing near.
due.
en route.
eventual.
expected.
fated.
foreseen.
forthcoming.
future.
gaining upon.
getting near.
immediate.
imminent.
impending.
in prospect.
in store.
in the offing.
in the wind.
in view.
instant.
marked.
near.
nearing.
next.
nigh.
oncoming.
ordained.
predestined.
preparing.
progressing.
prospective.
pursuing.
running after.
subsequent.
to be.
up-and-coming.
forthcoming.
adj. expected, imminent.
accessible.
anticipated.
approaching.
at hand.
available.
awaited.
coming.
destined.
fated.
future.
impending.
in evidence.
in preparation.
in prospect.
in store.
in the cards.
in the wind.
inescapable.
inevitable.
nearing.
obtainable.
on tap.
oncoming.
open.
pending.
predestined.
prospective.
ready.
resulting.
upcoming.
likely.
adj. probable, apt, hopeful.
acceptable.
achievable.
anticipated.
assuring.
attainable.
believeable.
conceivable.
conjecturable.
credible.
destined.
disposed.
expected.
fair.
favorite.
feasible.
given to.
imaginable.
in favor of.
in the cards.
in the habit of.
inclined.
inferable.
liable.
odds-on.
on the verge of.
ostensible.
plausible.
possible.
practicable.
predisposed.
presumable.
promising.
prone.
rational.
reasonable.
seeming.
subject to.
supposable.
tending.
thinkable.
true.
up-and-coming.
verisimilar.
workable.
natural.
adj. normal, everyday.
accustomed.
anticipated.
characteristic.
common.
commonplace.
congenital.
connatural.
consistent.
constant.
counted on.
customary.
essential.
familiar.
general.
habitual.
inborn.
indigenous.
ingenerate.
inherent.
innate.
instinctive.
intuitive.
involuntary.
legitimate.
logical.
looked for.
matter-of-course.
natal.
native.
ordinary.
prevailing.
prevalent.
probable.
reasonable.
regular.
relied on.
spontaneous.
typic.
typical.
unacquired.
uncontrolled.
uniform.
universal.
usual.
predictable.
adj. easy to foretell.
anticipated.
calculable.
certain.
expected.
foreseeable.
foreseen.
likely.
prepared.
sure.
sure-fire.
aimed.
adj. proposed.
anticipated.
calculated.
designed.
directed.
intended.
marked.
planned.
steered.
Roget's 21st Century Thesaurus, Third Edition Copyright © 2013 by the Philip Lief Group.
Cite This Source.
1 2 3 ... 5 NEXT.
 