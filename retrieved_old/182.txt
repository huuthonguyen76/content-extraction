JR Tower Hotel Nikko Sapporo.
1,380 Reviews.
#1 of 164 hotels / ryokan in Sapporo.
21/02/2017 “Great location, great service”.
13/02/2017 “Location - Location - Location ”.
Luxury Jouzankei Onsen.
Show Prices.
Check In Check Out.
Vessel Inn Sapporo Nakajima Park.
417 Reviews.
#2 of 164 hotels / ryokan in Sapporo.
13/02/2017 “Good value for money with superb breakfast ”.
11/02/2017 “Perfect location, but small”.
Mid-range.
Show Prices.
Check In Check Out.
Ibis Styles Sapporo.
293 Reviews.
#3 of 164 hotels / ryokan in Sapporo.
17/02/2017 “Pleasantly surprised ”.
13/02/2017 “Great place to stay in Sapporo”.
Mid-range Breakfast included.
Show Prices.
Check In Check Out.
Sapporo Park Hotel Offers & AnnouncementsBest Rate Guarantee.
439 Reviews.
#4 of 164 hotels / ryokan in Sapporo.
15/02/2017 “Hard pillow”.
25/12/2016 “Hotel with Fantastic Staff and Great Convenient.”.
Mid-range.
Show Prices.
Check In Check Out.
Mercure Hotel Sapporo.
1,072 Reviews.
#5 of 164 hotels / ryokan in Sapporo.
21/02/2017 “Clean, modern, enough space twin share”.
20/02/2017 “Great value!”.
Mid-range.
Show Prices.
Check In Check Out.
Cross Hotel Sapporo.
1,134 Reviews.
#6 of 164 hotels / ryokan in Sapporo.
08/02/2017 “Really lovely stay at the Cross Hotel”.
07/02/2017 “Great Location ”.
Mid-range.
Show Prices.
Check In Check Out.
Century Royal Hotel.
720 Reviews.
#7 of 164 hotels / ryokan in Sapporo.
14/02/2017 “Location. Location. Location.”.
13/02/2017 “Great location, good service.”.
Mid-range.
Show Prices.
Check In Check Out.
Hotel Keihan Sapporo.
702 Reviews.
#8 of 164 hotels / ryokan in Sapporo.
14/01/2017 “City stat”.
27/12/2016 “Nice hotel with good location”.
Mid-range.
Show Prices.
Check In Check Out.
Richmond Hotel Sapporo Ekimae.
528 Reviews.
#9 of 164 hotels / ryokan in Sapporo.
14/02/2017 “Worth Staying in Sapporo Downtown with Great Value”.
10/02/2017 “Perfect spot”.
Sapporo Station Mid-range.
Show Prices.
Check In Check Out.
Keio Plaza Hotel Sapporo.
903 Reviews.
#10 of 164 hotels / ryokan in Sapporo.
20/02/2017 “An amazing and enjoyable experience of snow in Hokkaido, Sapporo”.
15/02/2017 “Nice Experience”.
Mid-range Pool.
Show Prices.
Check In Check Out.
Mitsui Garden Hotel Sapporo.
571 Reviews.
#11 of 164 hotels / ryokan in Sapporo.
14/02/2017 “Helpful staff”.
14/02/2017 “Nice hotel and perfect location ”.
Mid-range.
Show Prices.
Check In Check Out.
Sapporo Grand Hotel Offers & AnnouncementsPackage Deal.
1,388 Reviews.
#12 of 164 hotels / ryokan in Sapporo.
10/02/2017 “Ski Tour”.
08/02/2017 “Good old Grand Hotel”.
Mid-range Nishi Juitchome Station.
Show Prices.
Check In Check Out.
Dormy Inn PREMIUM Sapporo.
382 Reviews.
#13 of 164 hotels / ryokan in Sapporo.
02/01/2017 “Cheap and cheerful”.
10/12/2016 “A good night stay”.
Mid-range Jouzankei Onsen.
Show Prices.
Check In Check Out.
Hotel Monterey Edelhof Sapporo Special OfferPackage Deal.
624 Reviews.
#14 of 164 hotels / ryokan in Sapporo.
16/02/2017 “Perfect location!”.
13/02/2017 “Enjoyable stay”.
Sapporo Station Mid-range.
Show Prices.
Check In Check Out.
Hotel Monterey Sapporo Special OfferPackage Deal.
774 Reviews.
#15 of 164 hotels / ryokan in Sapporo.
05/02/2017 “Convenient”.
29/01/2017 “Christmas in Sapporo”.
Sapporo Station Mid-range.
Show Prices.
Check In Check Out.
HOTEL MYSTAYS Sapporo Aspen.
526 Reviews.
#16 of 164 hotels / ryokan in Sapporo.
18/02/2017 “Great value, roomy ”.
14/02/2017 “Quality accommodation close to station”.
Sapporo Station Mid-range.
Show Prices.
Check In Check Out.
HOTEL MYSTAYS Sapporo Station.
612 Reviews.
#17 of 164 hotels / ryokan in Sapporo.
22/01/2017 “Great Hotel, very near to the Sapporo Station ”.
01/01/2017 “Nice comfortable stay at this nice hotel”.
Sapporo Station Mid-range.
Show Prices.
Check In Check Out.
Daiwa Roynet hotel Sapporo Susukino Offers & AnnouncementsBEST RATE GUARANTEE.
233 Reviews.
#18 of 164 hotels / ryokan in Sapporo.
20/02/2017 “100% come back”.
12/02/2017 “Amazing location ”.
Mid-range.
Show Prices.
Check In Check Out.
Furukawa Ryokan A Ryokan is a traditional Japanese accommodation which typically features ‘futon’ (folding mattresses) on ‘tatami’ (straw mat) floors. Many Ryokan are known for their public baths (typically "Onsen" hot springs) and their traditional cuisine. 24/7 front desk service is not guaranteed for this accommodation type.
205 Reviews.
#19 of 164 hotels / ryokan in Sapporo.
13/02/2017 “Rustic and traditional”.
27/01/2017 “Excellent onsen ”.
Pets Allowed Mid-range Free Parking.
Show Prices.
Check In Check Out.
Hotel Route Inn Sapporo Chuo Susukino.
271 Reviews.
#20 of 164 hotels / ryokan in Sapporo.
21/02/2017 “Clean, comfortable hotel”.
13/02/2017 “Helpful and accommodating staff, good location”.
Mid-range.
Show Prices.
Check In Check Out.
Hotel Mystays Premier Sapporo Park.
690 Reviews.
#21 of 164 hotels / ryokan in Sapporo.
15/02/2017 “Good Stay!”.
05/01/2017 “Nice Stay: Small Room--Onsen a Plus”.
Mid-range.
Show Prices.
Check In Check Out.
Sapporo Prince Hotel Special OfferBest deals & Free Wi-Fi.
606 Reviews.
#22 of 164 hotels / ryokan in Sapporo.
09/02/2017 “A reasonably priced hotel in a walking distance to the city center!!”.
04/01/2017 “Well maintained hotel.”.
Mid-range Nishi Juitchome Station.
Show Prices.
Check In Check Out.
Jozankei Tsuruga Resort Spa Mori No Uta.
220 Reviews.
#23 of 164 hotels / ryokan in Sapporo.
12/02/2017 “Best Onsen I ever had in Hokkaido”.
18/12/2016 “Review of hotel”.
Luxury Free Parking Breakfast included.
Show Prices.
Check In Check Out.
Richmond Hotel Sapporo-odori.
420 Reviews.
#24 of 164 hotels / ryokan in Sapporo.
27/12/2016 “Good location”.
17/10/2016 “Good Location”.
Mid-range.
Show Prices.
Check In Check Out.
Sapporo Excel Hotel Tokyu.
537 Reviews.
#25 of 164 hotels / ryokan in Sapporo.
16/02/2017 “Comfortable and convenient.”.
05/02/2017 “Convenient. Good size room. ”.
Mid-range.
Show Prices.
Check In Check Out.
La'gent Stay Sapporo Odori.
67 Reviews.
#26 of 164 hotels / ryokan in Sapporo.
19/02/2017 “Good choice”.
17/02/2017 “New hotel”.
Mid-range.
Show Prices.
Check In Check Out.
Watermark Hotel Sapporo.
212 Reviews.
#27 of 164 hotels / ryokan in Sapporo.
08/01/2017 “Great budget hotel in excellent location”.
17/08/2016 “Nice budget hotel”.
Susukino Station Mid-range.
Show Prices.
Check In Check Out.
Dormy Inn Sapporo Annex.
227 Reviews.
#28 of 164 hotels / ryokan in Sapporo.
02/02/2017 “Great for its price ”.
01/01/2017 “Definitely Good Value!”.
Susukino Station Mid-range.
Show Prices.
Check In Check Out.
New Otani Inn Sapporo Special OfferPackage Deal.
416 Reviews.
#29 of 164 hotels / ryokan in Sapporo.
17/01/2017 “1st 3 Nights In Sapporo”.
02/01/2017 “Booked through hotel.com”.
Sapporo Station Mid-range.
Show Prices.
Check In Check Out.
ANA Holiday Inn Sapporo Susukino.
348 Reviews.
#30 of 164 hotels / ryokan in Sapporo.
17/02/2017 “Good hotel located in Susukino”.
13/02/2017 “Great location”.
Mid-range.
Show Prices.
Check In Check Out.
